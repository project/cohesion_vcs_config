<?php

namespace Drupal\cohesion_vcs_config\EventSubscriber;

use Drupal\cohesion_sync\Config\CohesionFileStorage;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\StorageTransformEvent;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Acquia Site Studio - VCS Config event subscriber.
 */
class CohesionVcsConfigSubscriber implements EventSubscriberInterface {

  /**
   * Fields in config containing JSON string.
   */
  const JSON_FIELDS = [
    'json_values',
    'json_mapper',
    'settings',
  ];

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::STORAGE_TRANSFORM_IMPORT][] = ['onImportTransform'];
    $events[ConfigEvents::STORAGE_TRANSFORM_EXPORT][] = ['onExportTransform'];
    return $events;
  }

  /**
   * The storage is transformed for importing.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The config storage transform event.
   */
  public function onImportTransform(StorageTransformEvent $event) {
    $this->conversion($event, 'toJson');
  }

  /**
   * The storage is transformed for exporting.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The config storage transform event.
   */
  public function onExportTransform(StorageTransformEvent $event) {
    $this->conversion($event, 'fromJson');
  }

  /**
   * Updates Acquia Site Studio config storage according to direction.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   Config storage.
   * @param string $direction
   *   The direction of input data. 'fromJson' or 'toJson'.
   */
  protected function conversion(StorageTransformEvent $event, string $direction) : void {

    $storage = $event->getStorage();
    foreach ($storage->readMultiple($storage->listAll('cohesion')) as $name => $config) {

      // Fallback if there is no fields to override.
      if (!$this->hasFieldsToOverride($config)) {
        continue;
      }

      foreach (self::JSON_FIELDS as $jsonField) {
        if (empty($config[$jsonField]) || !is_string($config[$jsonField])) {
          continue;
        }
        switch ($direction) {
          case 'fromJson':
            $config[$jsonField] = CohesionFileStorage::prettyPrintJson($config[$jsonField]);
            break;

          case 'toJson':
            $config[$jsonField] = CohesionFileStorage::minifyJson($config[$jsonField]);
            break;
        }
      }

      $storage->write($name, $config);
    }
  }

  /**
   * Does array has JSON fields to override.
   *
   * @param array $config
   *   Config array.
   *
   * @return bool
   *   Indicator.
   */
  protected function hasFieldsToOverride(array $config) : bool {
    return (bool) count(array_intersect(array_keys($config), self::JSON_FIELDS));
  }

}
